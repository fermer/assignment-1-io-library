section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60     ; Системный вызов для exit
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax                ; Обнуляем счетчик длины
    mov rsi, rdi
    .loop_string_length:
        cmp byte [rsi], 0       ; Сравниваем текущий символ с нулевым символом (конц строки)
        je .end_string_length   ; Если достигнут конец строки, завершаем подсчет
        inc rax                 ; Увеличиваем счетчик длинны
        inc rsi                 ; Переходим к следующему символу в втроке
        jmp .loop_string_length ; Продолжаем подсчет
    .end_string_length:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdi, rsp
    call print_string
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10    ; Указатель на строку (символ новой срроки)
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rdi, 10
    mov rsi, rsp
    sub rsp, 32                 ; Reserve space on the stack
    dec rsi
    mov byte[rsi], 0
    .loop_print_uint:
        xor rdx, rdx
        div rdi             ; цифра
        add dl, '0'        ; ASCII
        dec rsi
	    mov byte[rsi], dl
        cmp rax, 0
        jz .end_print_uint
        jmp .loop_print_uint
    .end_print_uint:
        mov rdi, rsi
        call print_string

        add rsp, 32                 ; Free the allocated space on the stack
        ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jnl print_uint      ; если больше нуля можно как юинт
    push rdi
    mov rdi, '-'        ; отрываем минус
    call print_char
    pop rdi
    neg rdi
    jmp print_uint      ; теперь больше нуля, можно как юинт

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop_string_equals:
        mov bl, [rdi + rax]
        cmp bl, [rsi + rax]
        jne .not_eq_strings
        test bl, bl
        je .eq_strings
        inc rax                 ; сдвиг
        jmp .loop_string_equals
    .eq_strings:
        mov rax, 1
        ret
    .not_eq_strings:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, 0      ; Берем стандартные потоки, как для вывода
    mov rdi, 0      ;   только тут для ввода нужно
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14
    .loop_read_word:
        call read_char
        cmp rax, 0x20
        je .skip_read_word
        cmp rax, 0x9
        je .skip_read_word
        cmp rax, 0xA
        je .skip_read_word
        cmp r13, r14
        jng .bad_read_word
        test rax, rax
        je .good_read_word
        mov [r12 + r14], al
        inc r14
        jmp .loop_read_word
    .skip_read_word:
        test r14, r14
        je .loop_read_word
    .good_read_word:
        mov byte[r12 + r14], 0
        mov rax, r12
        mov rdx, r14
        jmp .end_read_word
    .bad_read_word:
        xor rax, rax
    .end_read_word:
        pop r14         ; Восстанавливать нужно в обратом порядке из-за устройства стека
        pop r13
        pop r12
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx
    xor r8, r8
    xor rax, rax
    mov r9, 10
    .loop_parse_uint:
        mov r8b, byte[rdi + rdx]
        cmp r8b, '0'            ; Проверка цифра ли вообще (4 строки проверки)
        jl .end_parse_uint
        cmp r8b, '9'
        jg .end_parse_uint
        push rdx
        mul r9                  ; Когда читали сдвигали делением, тут мультиплай
        pop rdx
        sub r8, '0'
        add rax, r8
        inc rdx
        jmp .loop_parse_uint
    .end_parse_uint:
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi], '-'                ; Моэно не кодом а символом!!
    je .negative_parse_int
    cmp byte[rdi], '+'
    je .positive_parse_int
    jmp parse_uint
    .negative_parse_int:        ; нет знака нет проблем
        inc rdi
        push rdi
        call parse_uint
        pop rdi
        neg rax                 ; ну может только нужно число сделать -x
        inc rdx
        ret
    .positive_parse_int:        ; нет знака нет проблем
        inc rdi
        push rdi
        call parse_uint
        pop rdi
        inc rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop_string_copy:
        mov cl, byte[rdi + rax]
        inc rax
        cmp rax, rdx
        jg .bad_string_copy
        mov byte[rsi + rax - 1], cl
        cmp cl, 0
        je .end_string_copy
        jmp .loop_string_copy
    .bad_string_copy:
        xor rax, rax
    .end_string_copy:
        ret
